# C-CNC Project

Simplicistic CNC code written in C, for the course of Manufacturing Automation, University of Trento, Department of Industrial Engineering.


## Building instuctions

Building the application takes two steps: building the `ccnc` main executable and building the 3D viewe application, `MTViewer`.

### Building the viewer
```sh
$ cd MTViewer
$ mkdir build
$ cd build
$ cmake ..
$ make install
```
This creates the `bin/MTViewer` executable. Try and launch it to se the 3-axes machine tool. You can use the `x`, `y`, and `z` keys for moving the three axes towards positive directions, and `X`, `Y`, and `Z` for the negative directions.

### Building the main code
```sh
$ mkdir build
$ cd build
$ cmake ..
$ make install 
```
This creates the main executable into `bin/ccnc`. Assuming that you are in project root folder, it can be launched as:
```sh
$ bin/ccnc test.g
```
where `test.g` is the sample G-code program. This automatically opens the viewer.