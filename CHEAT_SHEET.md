# Cheat Sheet - basic terminal commands 

**Warning**: all commands are case-sensitive!

**Note**: In the following, `<arg>` indicates a mandatory argument, while `[arg]` indicates an optional argument.

## Linux set-up
The following commands have to be executed **only if you are setting up a Linux machine (virtual or real) for the first time**:

```sh
$ sudo apt update
$ sudo apt install clang lldb gdb cmake git
# Only for building the MTViewer interface: 
$ sudo apt install freeglut3-dev libxmu-dev libxi-dev libreadline7 libreadline-dev
```

The following command updates the system for using the `clang` compiler rather than the default `gcc`:

```sh
$ sudo update-alternatives --set cc /usr/bin/clang
```

If you need to change the keyboard layout for a virtual machine that lacks a GUI window for this (as for BunsenLabs Linux VM), you can run the following command from terminal:

```sh
$ sudo dpkg-reconfigure keyboard-configuration
```

Finally, download Visual Studio Code from [https://code.visualstudio.com/download](https://code.visualstudio.com/download) (for Ubuntu and Debian-like distributions, select the `.deb` package), install it and add the following plugins:

- C/C++, from Microsoft
- CMake, from twxs
- CMake Tools, from vector-of-bool
- Lua, from keyring


## In-line help
You can have in-line help from the console with the `man` command (short for *manual*). The `man` command provides content that is organized in *sections*. Section `1` is for shell commands, section `3` is for C functions.

For example:

- if you want help on the `ls` shell command, type: `man 1 ls`, or simply `man ls` (if you omit the section, it will default to section `1`).
- if you want help on the `printf()` function in C, type: `man 3 printf`.


## Filesystem
- Path shortcuts: `.` is the current directory, `..` is its parent directory, `~` (on Linux and Italian keyboard: `AltGr+ì`) the home folder
- Show the path of the current directory: `pwd`
- Listing a directory: `ls [dir]` (short) or `ls -l [dir]` (full table); if `dir` is missing, lists the current directory
- Changing to a directory: `cd [path]`. If `path` is missing, changes to the home folder
- Creating a directory: `mkdir <dir>`
- Remove a file (**irreversible!**): `rm <file>`
- Remove a directory and its content (**irreversible!**): `rm -rf <path>`
- Copy a file: `cp <source_path> <dest_path>`
- Copy a directory: `cp -R <source_dir> <dest_path>` (*note*: `-R` is capital)
- Move a file: `mv <source_path> <dest_path>`
- Move a directory: `mv -R <source_dir> <dest_path>` (*note*: `-R` is capital)

## CMake-based development
- All build products and intermediate files are put on the `build` subdirectory of the project root
- If not existing, create it `mkdir build`
- For *configuring* the project: `cmake ..` from within `build`
- The CMake configuration must be re-executed if you change the `CMakeLists.txt` file or if you add some source file to your project
- At any time, you can re-configure from scratch: `cd build; rm -rf *; cmake ..`
- For *building* the project: `make` from within `build`
- For *installing* the project products: `make install` from within `build`
- If your build system behaves in an unexpected way, try to *re-configure from scratch*
