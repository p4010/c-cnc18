//   ____
//  |  _ \ _ __ ___   __ _ _ __ __ _ _ __ ___
//  | |_) | '__/ _ \ / _` | '__/ _` | '_ ` _ \
//  |  __/| | | (_) | (_| | | | (_| | | | | | |
//  |_|   |_|  \___/ \__, |_|  \__,_|_| |_| |_|
//                   |___/
// program interface

#ifndef PROGRAM_H
#define PROGRAM_H
#include "block.h"
#include "ccnc.h"
#include <machine.h>

enum block_ctrl { STOP = 0, CONTINUE };

// Program structure
struct program {
  char *filename;        // G-code source file
  FILE *file;            // G-code filehandle
  struct block **blocks; // Array of block
  index_t n;             // number of parsed blocks
};

// Loop callback prototype
typedef enum block_ctrl (*loop_cbk)(struct block *b, data_t t, void *userdata);
typedef void (*block_cbk)(struct block *b, void *userdata);

// Create a new program from the G-Code in 'filename'
struct program *program_new(const char *filename);

// Deallocate program and its resources
void program_free(struct program *program);

// Parse G-code program
void program_parse(struct program *program, struct machine_config *cfg);

// Print a description of parsed program to the filehandle 'out'
void program_print(struct program *program, FILE *out);

// Loop on the whole program, calling 'b_cbk' at the beginning of
// each block, and 'l_cbk' at every timestep
void program_loop(struct program *p, struct machine_config *cfg,
                  block_cbk b_cbk, loop_cbk l_cbk, void *userdata);

// Update start coordinates of the first block in program
void program_set_start(struct program *p, point_t *pt);

#endif