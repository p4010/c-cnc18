//    ____       ____ _   _  ____
//   / ___|     / ___| \ | |/ ___|
//  | |   _____| |   |  \| | |
//  | |__|_____| |___| |\  | |___
//   \____|     \____|_| \_|\____|
// This is the common project header file
// Declare and define here types and structures that are of common usage

// Double inclusion guard
#ifndef CCNC_H
#define CCNC_H

// System headers to be included
#define _GNU_SOURCE // also enables asprintf() in stdio.h
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// C preprocessor definitions
// They play the role of CONSTANTS. Any occurrence of e.g. TQ in code
// including this header is replaced with the corresponding value (e.g. 0.005)
// immediately before the compilation.
// #define TQ 0.005
// #define A 10
// #define D 20

// Data types
typedef double data_t;    // for REAL data
typedef uint32_t index_t; // for indexes

#endif
