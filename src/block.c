// Block functions implementations
#include "block.h"
#include <ctype.h> // needed for toupper() function

// BEGIN OF STATIC FUNCTIONS
// static functions are only available from this file
static data_t quantize(data_t t, data_t tq, data_t *dq) {
  data_t q;
  q = ((index_t)(t / tq) + 1) * tq;
  *dq = q - t;
  return q;
}

static void block_precompute(struct block *b) {
  assert(b != NULL);
  data_t A = b->config->A;
  data_t D = b->config->D;
  data_t dt, dq, a, d;
  data_t f_m = b->feedrate / 60.0;
  data_t l = block_length(b);
  data_t dt_1 = f_m / A;
  data_t dt_2 = f_m / D;
  data_t dt_m = l / f_m - (dt_1 + dt_2) / 2.0;
  if (dt_m > 0) { // trapezoidal profile
    dt = quantize(dt_1 + dt_m + dt_2, b->config->tq, &dq);
    dt_m += dq;
    f_m = (2 * l) / (dt_1 + dt_2 + 2 * dt_m);
  } else { // triangular profile
    dt_1 = sqrt(2 * l / (A + pow(A, 2) / D));
    dt_2 = dt_1 * A / D;
    dt = quantize(dt_1 + dt_2, b->config->tq, &dq);
    dt_m = 0;
    dt_2 += dq;
    f_m = 2 * l / (dt_1 + dt_2);
  }
  a = f_m / dt_1;
  d = -(f_m / dt_2);

  b->prof = (struct block_profile *)malloc(sizeof(struct block_profile));
  assert(b->prof != NULL);
  memset(b->prof, 0, sizeof(struct block_profile));
  b->prof->dt_1 = dt_1;
  b->prof->dt_2 = dt_2;
  b->prof->dt_m = dt_m;
  b->prof->a = a;
  b->prof->d = d;
  b->prof->f = f_m;
  b->prof->dt = dt;
  b->prof->l = l;
}

static void block_set_field(struct block *b, char f, char *v) {
  assert(b != NULL);
  switch (f) { // argument of switch can only be int or char
  case 'N':
    b->n = atol(v); // convert v to a long int
    break;
  case 'G':
    b->type = (enum block_type)atoi(v); // convert v to an int
    break;
  case 'X':
    point_x(&b->target, atof(v)); // copy float value of v to X
    break;
  case 'Y':
    point_y(&b->target, atof(v));
    break;
  case 'Z':
    point_z(&b->target, atof(v));
    break;
  case 'F':
    b->feedrate = atof(v);
    break;
  case 'S':
    b->spindle = atof(v);
    break;
  case 'T':
    b->tool = atoi(v);
    break;
  default: // not any of the previous cases!
    printf("Unexpected token: %c%s\n", f, v);
  }
}
// END OF STATIC FUNCTIONS

struct block *block_new(char *line, struct block *prev) {
  struct block *b;

  b = (struct block *)malloc(sizeof(struct block));
  assert(b != NULL); // stop if maccloc() didn't work!

  if (prev != NULL) {
    // copy all values of prev block into current one
    // inheriting all fields that are not defined
    memcpy(b, prev, sizeof(struct block));
    b->prev = prev;
  } else {
    b->prev = NULL;
  }
  b->target = point_new();
  b->prof = NULL;
  b->delta = point_new();
  b->config = NULL;
  b->tool = 0;
  asprintf(&b->line, "%s", line);
  return b;
}

void block_free(struct block *block) {
  assert(block != NULL);
  // free all referenced objects in block before freeing block itself
  if (block->line != NULL) {
    free(block->line);
  }
  if (block->prof != NULL) {
    free(block->prof);
  }
  free(block);
}

index_t block_parse(struct block *block) {
  assert(block != NULL);
  index_t i = 0;
  char *token = NULL, *line = NULL, *to_free = NULL;

  to_free = line = strdup(block->line);
  assert(line != NULL); // check that strdup() worked

  while ((token = strsep(&line, " ")) != NULL) {
    block_set_field(block, toupper(token[0]), token + 1);
    i++;
  }
  free(to_free);

  if (block->prev != NULL) {
    point_modal(&block->target, &block->prev->target);
    point_delta(&block->prev->target, &block->target, &block->delta);
  }

  block_precompute(block);
  return i;
}

data_t block_length(struct block *b) {
  assert(b != NULL);
  data_t result = 0;
  if (b->prev != NULL) {
    result = point_dist(&b->prev->target, &b->target);
  } else {
    point_t zero = point_new();
    point_xyz(&zero, b->config->zero[0], b->config->zero[1],
              b->config->zero[2]);
    result = point_dist(&zero, &b->target);
  }
  return result;
}

data_t block_lambda(struct block *b, data_t t) {
  assert(b != NULL);
  data_t r;
  data_t dt_1 = b->prof->dt_1;
  data_t dt_2 = b->prof->dt_2;
  data_t dt_m = b->prof->dt_m;
  data_t a = b->prof->a;
  data_t d = b->prof->d;
  data_t f = b->prof->f;

  if (t < 0) {
    r = 0.0;
  } else if (t < dt_1) {
    r = a * pow(t, 2) / 2.0;
  } else if (t < (dt_1 + dt_m)) {
    r = f * (dt_1 / 2.0 + (t - dt_1));
  } else if (t < (dt_1 + dt_m + dt_2)) {
    data_t t_2 = dt_1 + dt_m;
    r = f * dt_1 / 2.0 + f * (dt_m + t - t_2) +
        d / 2.0 * (pow(t, 2) + pow(t_2, 2)) - d * t * t_2;
  } else {
    r = b->prof->l;
  }
  return r / b->prof->l; // normalize in [0,1]
}

point_t block_interpolate(struct block *b, data_t lambda) {
  assert(b != NULL);
  point_t result = point_new();
  if (b->prev != NULL) {
    point_t p0 = b->prev->target;
    point_x(&result, p0.x + b->delta.x * lambda);
    point_y(&result, p0.y + b->delta.y * lambda);
    point_z(&result, p0.z + b->delta.z * lambda);
  } else { // very first block
    point_x(&result, b->config->zero[0] + b->delta.x * lambda);
    point_y(&result, b->config->zero[1] + b->delta.y * lambda);
    point_z(&result, b->config->zero[2] + b->delta.z * lambda);
  }
  return result;
}

void block_print(struct block *b, FILE *out) {
  assert(b != NULL);
  char *t, *p;
  point_inspect(&t, &b->target);
  if (b->prev != NULL) {
    point_inspect(&p, &b->prev->target);
  } else {
    point_t zero = point_new();
    point_xyz(&zero, 0, 0, 0);
    point_inspect(&p, &zero);
  }
  fprintf(out, "%03d: %s->%s F%07.1f S%07.1f T%02d %d\n", b->n, p, t,
          b->feedrate, b->spindle, b->tool, b->type);
  free(t);
  free(p);
}

void block_set_start(struct block *b, point_t *p) {
  b->config->zero[0] = p->x;
  b->config->zero[1] = p->y;
  b->config->zero[2] = p->z;
  block_precompute(b);
}
