//  _____     _     _
// |  _  |___|_|___| |_
// |   __| . | |   |  _|
// |__|  |___|_|_|_|_|

#include "point.h"

// usage:
// point_t p1 = point_new();
point_t point_new() {
  point_t p = {0, 0, 0};
  p.s = 0;
  return p;
}

#define X_SET '\1'
#define Y_SET '\2'
#define Z_SET '\4'
#define ALL_SET '\7'

// BITMASK:
// p->s: xxxx xxxx
// '\1': 0000 0001
// ---------------
// OR:   xxxx xxx1
void point_x(point_t *p, data_t v) {
  p->x = v;
  p->s |= X_SET;
}

// BITMASK:
// p->s: xxxx xxxx
// '\2': 0000 0010
// ---------------
// OR:   xxxx xx1x
void point_y(point_t *p, data_t v) {
  p->y = v;
  p->s |= Y_SET;
}

// BITMASK:
// p->s: xxxx xxxx
// '\4': 0000 0100
// ---------------
// OR:   xxxx x1xx
void point_z(point_t *p, data_t v) {
  p->z = v;
  p->s |= Z_SET;
}

void point_xyz(point_t *p, data_t x, data_t y, data_t z) {
  p->x = x;
  p->y = y;
  p->z = z;
  p->s |= ALL_SET;
}

data_t point_dist(point_t *p1, point_t *p2) {
  return sqrt(pow(p2->x - p1->x, 2) + pow(p2->y - p1->y, 2) +
              pow(p2->z - p1->z, 2));
}

void point_delta(point_t *p1, point_t *p2, point_t *delta) {
  assert((p1->s == ALL_SET) && (p2->s == ALL_SET));
  // delta->x = p2->x - p1->x;
  // delta->y = p2->y - p1->y;
  // delta->z = p2->z - p1->z;
  // delta->s = ALL_SET;
  // or, briefly:
  point_xyz(delta, p2->x - p1->x, p2->y - p1->y, p2->z - p1->z);
}

void point_modal(point_t *p1, point_t *p2) {
  if (!(p1->s & X_SET) && (p2->s & X_SET)) {
    point_x(p1, p2->x);
  }
  if (!(p1->s & Y_SET) && (p2->s & Y_SET)) {
    point_y(p1, p2->y);
  }
  if (!(p1->s & Z_SET) && (p2->s & Z_SET)) {
    point_z(p1, p2->z);
  }
}

void point_inspect(char **result, point_t *p) {
  if (p == NULL) { // return something like "[ - - - ]"
    asprintf(result, "[%8s %8s %8s]", "-", "-", "-");
  } else {
    char *s; // support string
    // (re)allocate necessary space in result:
    *result = (char *)calloc(28 + 1, sizeof(char));

    if (p->s & X_SET) {
      asprintf(&s, "[%8.3f ", p->x);
    } else {
      asprintf(&s, "[%8s ", "-");
    }
    strcat(*result, s);

    if (p->s & Y_SET) {
      asprintf(&s, "%8.3f ", p->y);
    } else {
      asprintf(&s, "%8s ", "-");
    }
    strcat(*result, s);

    if (p->s & Z_SET) {
      asprintf(&s, "%8.3f]", p->z);
    } else {
      asprintf(&s, "%8s]", "-");
    }
    strcat(*result, s);

    // release s memory and avoid memory leaks!
    free(s);
  }
}