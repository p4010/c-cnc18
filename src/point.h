//   ____       _       _
//  |  _ \ ___ (_)_ __ | |_
//  | |_) / _ \| | '_ \| __|
//  |  __/ (_) | | | | | |_
//  |_|   \___/|_|_| |_|\__|
// point in 3D coordinates

// Double inclusion guard:
#ifndef POINT_H
#define POINT_H

// include the common header
#include "ccnc.h"

// a C struct representing a point in 3D coordinates
typedef struct {
  data_t x, y, z; // coordinates
  uint8_t s;      // bitmask for set/unset fields
} point_t;

//  _____             _   _
// |   __|_ _ ___ ___| |_|_|___ ___ ___
// |   __| | |   |  _|  _| | . |   |_ -|
// |__|  |___|_|_|___|_| |_|___|_|_|___|

// create a new point
point_t point_new();

// set the x field in a point
void point_x(point_t *p, data_t v);
// set the y field in a point
void point_y(point_t *p, data_t v);
// set the z field in a point
void point_z(point_t *p, data_t v);
// set all the fields in a point
void point_xyz(point_t *p, data_t x, data_t y, data_t z);

// Returns the distance between two points (always positive)
data_t point_dist(point_t *p1, point_t *p2);

// Calculate the projections of the segment from p1 to p2
// and return the result in delta.
// WARNING: order of p1 and p2 matters
void point_delta(point_t *p1, point_t *p2, point_t *delta);

// Copy values in p2 to undefined values in p1
void point_modal(point_t *p1, point_t *p2);

// Provide a description of p
// Warning: this function allocates result, which has to be freed once done!
void point_inspect(char **result, point_t *p);

#endif