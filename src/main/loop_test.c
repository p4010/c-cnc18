#include "../block.h"
#include "../point.h"
#include "../program.h"
#include <machine.h>

enum block_ctrl loop(struct block *b, data_t t, void *userdata) {
  assert(userdata != NULL);
  FILE *output = (FILE *)userdata;
  // wait_next(b->config->tq * 1E9);
  if (b->type == LINE) {
    data_t lambda = block_lambda(b, t);
    point_t pt = block_interpolate(b, lambda);
    fprintf(output, "%f %f %f %f %f\n", t, lambda, pt.x, pt.y, pt.z);
    return CONTINUE;
  } else {
    return STOP;
  }
}

void block_start(struct block *b, void *userdata) { block_print(b, stderr); }

int main(int argc, char const *argv[]) {
  struct program *p = NULL;
  struct machine *m = NULL;
  FILE *output = NULL;

  if (argc != 3) {
    printf("I need the configuration and the G-code file name!\n");
    exit(EXIT_FAILURE);
  }

  m = machine_new((char *)argv[1]);
  p = program_new(argv[2]);
  program_parse(p, m->cfg);
  printf("Parsed program for %s\n", argv[2]);
  program_print(p, stderr);

  printf("Program loop:\n");
  output = fopen("out.txt", "w");
  fprintf(output, "t lambda x y z\n");
  program_loop(p, m->cfg, block_start, loop, output);

  fclose(output);

  program_free(p);
  return 0;
}
