#include "../block.h"
#include "../point.h"
#include "../program.h"
#include <libgen.h> // for dirname()
#include <machine.h>
#include <sys/param.h> // for MAXPATHLEN var

enum block_ctrl loop(struct block *b, data_t t, void *userdata) {
  assert(userdata != NULL);
  struct machine *m = (struct machine *)userdata;
  point_t *position = NULL;
  data_t error = 0;
  static data_t ct = 0;
  enum block_ctrl result = CONTINUE;

  // wait for the next time tick
  wait_next(m->cfg->tq * 1E9); // 1E9 = 1*10^9
  ct += m->cfg->tq;

  // deal with different G-code block types
  switch (b->type) {
  case RAPID: {
    position = &b->target;
    break;
  }
  case LINE: {
    data_t l = block_lambda(b, t);
    point_t p = block_interpolate(b, l);
    position = &p;
    break;
  }
  case ARC_CW: {
    fprintf(stderr, "*** WARNING: CW arc interpolation not supported!\n");
    return STOP;
  }
  case ARC_CCW: {
    fprintf(stderr, "*** WARNING: CCW arc interpolation not supported!\n");
    return STOP;
  }
  default: {
    fprintf(stderr, "*** WARNING: Unsupported block type: %s\n", b->line);
    return STOP;
  }
  }

  // Solve for machine tool dynamics
  machine_go_to(m, position->x, position->y, position->z);
  machine_do_step(m, m->cfg->tq);
  error = machine_error(m);

  // Print position and values
  fprintf(stdout, "%3d %1d %7.3f %7.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n",
          b->n, b->type, ct, error, position->x, position->y, position->z,
          m->x->x, m->y->x, m->z->x);

  // Stop (and go to next block) if rapid movement and error less than threshold
  if (b->type == RAPID && error <= m->cfg->error) {
    result = STOP;
  }

  return result;
}

void block_start(struct block *b, void *userdata) { block_print(b, stderr); }

int main(int argc, char const *argv[]) {
  struct program *p = NULL;
  struct machine *m = NULL;
  struct machine_config *cfg = NULL;
  char this_dir[MAXPATHLEN] = "";
  char config_path[MAXPATHLEN] = "";
  char viewer_path[MAXPATHLEN] = "";
  point_t start;

  fprintf(stderr, "This is %s\n", argv[0]);
  strncpy(this_dir, dirname((char *)argv[0]), MAXPATHLEN);
  // strcpy(this_dir, dirname(argv[0])); // UNSAFE variant
  sprintf(config_path, "%s/config.lua", this_dir);
  sprintf(viewer_path, "%s/MTViewer", this_dir);
  fprintf(stderr, "Config: %s\nViewer: %s\n", config_path, viewer_path);

  // Check for proper number of arguments
  if (argc != 2) {
    fprintf(stderr, "I need the G-code file name!\n");
    exit(EXIT_FAILURE);
  }

  // Initialize machine and program objects
  m = machine_new(config_path);
  cfg = m->cfg;
  p = program_new(argv[1]);

  // Print the program description
  program_parse(p, cfg);
  fprintf(stderr, "Parsed program for %s\n", argv[1]);
  program_print(p, stderr);

  // Preparing for run
  machine_reset(m);
  machine_set_position(m, cfg->zero[0], cfg->zero[1], cfg->zero[2]);
  machine_enable_viewer(m, viewer_path);
  sleep(1);

  // Wait for the user pressing spacebar for starting the program
  fprintf(stderr, "Hit spacebar in the viewer to start!\n");
  machine_wait_for_run(m);

  // set current viewer position to part-program
  point_xyz(&start, m->viewer->coord[0], m->viewer->coord[1],
            m->viewer->coord[2]);
  program_set_start(p, &start);
  machine_set_position_from_viewer(m);

  // Run loop
  fprintf(stderr, "Program loop:\n");
  program_loop(p, m->cfg, block_start, loop, m);

  program_free(p);
  machine_free(m);
  return 0;
}
