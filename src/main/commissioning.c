// Controllers Commissioning executable
#include "../block.h"
#include "../point.h"
#include <libgen.h>
#include <machine.h>
#include <sys/param.h>

int main(int argc, char const *argv[]) {
  struct machine *m = NULL;
  struct axis *a = NULL;
  char cfg_path[MAXPATHLEN] = "";
  point_t start, stop;
  data_t t = 0;
  FILE *of = fopen("tuning.txt", "w");

  strncat(cfg_path, dirname((char *)argv[0]), MAXPATHLEN);
  strncat(cfg_path, "/config.lua", MAXPATHLEN);

  m = machine_new(cfg_path);
  if (argc != 2) {
    fprintf(stderr, "I need the axis name (x, y, or z)!\n");
    return EXIT_FAILURE;
  }

  // Deal with command line argument
  if (strncmp(argv[1], "x", 1) == 0) { // X axis
    a = m->x;
  } else if (strncmp(argv[1], "y", 1) == 0) {
    a = m->y;
  } else if (strncmp(argv[1], "z", 1) == 0) {
    a = m->z;
  } else {
    fprintf(stderr, "Unknown axis %s\n", argv[1]);
    return EXIT_FAILURE;
  }

  // define two points
  start = point_new();
  point_xyz(&start, 0, 0, 0);
  stop = point_new();
  point_xyz(&stop, 1, 1, 1);

  // Reset machine and set target to unit step
  machine_reset(m); // sets to 0,0,0 at zero speed
  machine_set_position(m, start.x, start.y, start.z);
  machine_go_to(m, stop.x, stop.y, stop.z);

  // print output to file
  fprintf(of, "t x v p i d o s e\n");
  for (t = 0; t < 20; t += m->cfg->tq) {
    machine_do_step(m, m->cfg->tq);
    fprintf(of, "%f %f %f %f %f %f %f %d %f\n", t, a->x, a->v, a->pid->p,
            a->pid->i, a->pid->d, a->pid->output, a->pid->saturate,
            pid_error(a->pid));
  }

  // cleanup
  fclose(of);
  machine_free(m);
  return 0;
}
