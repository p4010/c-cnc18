// PROGRAM FOR TESTING SAKE
#include "../block.h"
#include "../point.h"

int main(int argc, char const *argv[]) {
  // In C, lone curly braces serve to delimit a local scope:
  // all variables declared within a scope er only visible wihin
  // that scope (and are not visible outside it)
  { // Testing points
    // variables declarations
    point_t p0, p1, p2, p3;
    char *p_desc = NULL;

    // Create new points
    p0 = point_new();
    p1 = point_new();
    p2 = point_new();
    p3 = point_new();

    printf("After creation:\n");
    point_inspect(&p_desc, &p0);
    printf("p0: %s\n", p_desc);
    point_inspect(&p_desc, &p1);
    printf("p1: %s\n", p_desc);
    point_inspect(&p_desc, &p2);
    printf("p2: %s\n", p_desc);

    // set some values
    point_xyz(&p0, 0, 0, 0); // origin
    point_x(&p1, 100.0);
    point_z(&p1, 12.0);
    point_xyz(&p2, 0, 0, 5);

    printf("After setting values:\n");
    point_inspect(&p_desc, &p0);
    printf("p0: %s\n", p_desc);
    point_inspect(&p_desc, &p1);
    printf("p1: %s\n", p_desc);
    point_inspect(&p_desc, &p2);
    printf("p2: %s\n", p_desc);

    // testing point_modal():
    printf("After point_modal():\n");
    point_modal(&p1, &p0);
    point_inspect(&p_desc, &p1);
    printf("p1: %s\n", p_desc);

    // testing point_delta(), for which order is relevant
    printf("Projections (order is relevant!):\n");
    point_delta(&p1, &p2, &p3);
    point_inspect(&p_desc, &p3);
    printf("p3: %s (p1 -> p2)\n", p_desc);
    point_delta(&p2, &p1, &p3);
    point_inspect(&p_desc, &p3);
    printf("p3: %s (p2 -> p1)\n", p_desc);

    // testing point_dist()
    printf("Distances (order is irrelevant):\n");
    printf("p2 - p1 = %.3f\n", point_dist(&p1, &p2));
    printf("p1 - p2 = %.3f\n", point_dist(&p2, &p1));

    free(p_desc);
  }

  { // Testing blocks:
    struct block *b1, *b2, *b3;
    char *p_desc = NULL;

    // Create three blocks, in chain
    b1 = block_new("N01 X0 Y0 Z0 T1", NULL);
    block_parse(b1);
    b2 = block_new("N2 G00 Z250 F1000 S5000", b1);
    block_parse(b2);
    b3 = block_new("N03 G01 X100 y50", b2);
    block_parse(b3);

    // visualize b3
    point_inspect(&p_desc, &b3->target);
    block_print(b1, stdout);
    block_print(b2, stdout);
    block_print(b3, stdout);

    // testing interpolation on b3
    data_t t = 0, dt = 0.005;
    data_t lambda = 0;
    point_t p;

    FILE *f = fopen("data.txt", "w");

    while (lambda < 1.0) {
      lambda = block_lambda(b3, t);
      p = block_interpolate(b3, lambda);
      fprintf(f, "%f %f %f %f %f\n", t, lambda, p.x, p.y, p.z);
      t += dt;
    }

    fclose(f);

    // Free resources
    block_free(b1);
    block_free(b2);
    block_free(b3);
    free(p_desc);
  }

  return 0;
}
