// program
#include "program.h"
#define ALLOC_CHUNKS 10

struct program *program_new(const char *filename) {
  assert(filename != NULL);
  assert(strlen(filename) != 0);
  struct program *p = (struct program *)malloc(sizeof(struct program));
  assert(p != NULL);
  asprintf(&p->filename, "%s", filename);
  p->file = fopen(p->filename, "r");
  if (p->file == NULL) {
    printf("Cannot open file %s!\n", p->filename);
    return NULL;
  }
  p->blocks = NULL;
  p->n = 0;
  return p;
}

void program_free(struct program *p) {
  assert(p != NULL);
  free(p->filename);
  fclose(p->file);
  if (p->blocks != NULL) {
    index_t i;
    for (i = 0; i < p->n; i++) {
      block_free(p->blocks[i]);
    }
  }
  free(p);
}

void program_parse(struct program *p, struct machine_config *cfg) {
  assert(p != NULL);
  char *line = NULL;
  size_t n = 0;
  ssize_t line_len;
  index_t i = 0;

  p->blocks = calloc(ALLOC_CHUNKS, sizeof(struct block *));
  p->n = ALLOC_CHUNKS;
  while ((line_len = getline(&line, &n, p->file)) > 0) {
    // shortcut conditional:
    // (condition) ? (result if true) : (result if false)
    p->blocks[i] = block_new(line, i == 0 ? NULL : p->blocks[i - 1]);
    p->blocks[i]->config = cfg;
    block_parse(p->blocks[i]);
    i++;
    if (i > p->n) {
      p->n += ALLOC_CHUNKS;
      p->blocks = realloc(p->blocks, p->n * sizeof(struct block *));
      assert(p->blocks != NULL);
    }
  }
  p->n = i;
  p->blocks = realloc(p->blocks, p->n * sizeof(struct block *));
  assert(p->blocks != NULL);
}

void program_print(struct program *p, FILE *out) {
  assert(p != NULL);
  index_t i = 0;
  for (i = 0; i < p->n; i++) {
    block_print(p->blocks[i], out);
  }
}

void program_loop(struct program *p, struct machine_config *cfg,
                  block_cbk b_cbk, loop_cbk l_cbk, void *userdata) {
  assert(p != NULL);
  index_t i;
  data_t t;
  for (i = 0; i < p->n; i++) {
    struct block *b = p->blocks[i];
    if (b_cbk != NULL) {
      b_cbk(b, userdata);
    }
    for (t = 0.0; t < b->prof->dt; t += cfg->tq) {
      if (l_cbk(b, t, userdata) == STOP) {
        break;
      }
    }
  }
}

void program_set_start(struct program *p, point_t *pt) {
  block_set_start(p->blocks[0], pt);
}