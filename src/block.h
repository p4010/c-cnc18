//   ____  _            _
//  | __ )| | ___   ___| | __
//  |  _ \| |/ _ \ / __| |/ /
//  | |_) | | (_) | (__|   <
//  |____/|_|\___/ \___|_|\_\
// This is the header for G-code block functions
// a G-code block has this structure/syntax:
// N01 G00 X0 Y0 Z0 F5000 S8000 T1
#ifndef BLOCK_H
#define BLOCK_H

#include "ccnc.h"
#include "point.h"
#include <machine.h>

// Define the list of allowed motion commands
enum block_type {
  RAPID = 0, // G00
  LINE,      // G01
  ARC_CW,    // G02
  ARC_CCW,   // G03
  NO_MOTION  // anything else
};

// struct that holds data relevant for calculating the
// velocity profile
struct block_profile {
  data_t a, d;             // Max accelerations
  data_t f, l;             // feedrate and length
  data_t dt_1, dt_m, dt_2; // durations
  data_t dt;               // sampling time
};

// struct that describes a block
struct block {
  char *line;                    // copy of the original G-code line
  enum block_type type;          // type of motion
  index_t n;                     // block number
  index_t tool;                  // tool to be used
  point_t target;                // target position (at the end of block)
  point_t delta;                 // projections of current displacement
  data_t spindle;                // spindle rotation speed (rpm)
  data_t feedrate;               // feedrate (mm/min)
  struct machine_config *config; // machine tool configuration
  struct block_profile *prof;    // reference to the velocity profile structure
  struct block *prev;            // reference to the previous block
};

//  _____             _   _
// |   __|_ _ ___ ___| |_|_|___ ___ ___
// |   __| | |   |  _|  _| | . |   |_ -|
// |__|  |___|_|_|___|_| |_|___|_|_|___|

// Create a new block, allocated on the heap memory.
// line is the G-code string
// prev is a reference to the previous block
struct block *block_new(char *line, struct block *prev);

// Deallocate the memory of a block.
void block_free(struct block *block);

// Parse the block->line string and set the corresponding fields of block.
// Fields missing in block->line are inherited from block->prev
index_t block_parse(struct block *block);

// Return the length of the block.
data_t block_length(struct block *block);

// Return the value of lambda(time) for the current block.
data_t block_lambda(struct block *block, data_t time);

// Return the point position of the three axes at the given value of lambda.
point_t block_interpolate(struct block *b, data_t lambda);

// Print a deacription of the block.
void block_print(struct block *b, FILE *out);

// Update starting position for the very first
// block in program
void block_set_start(struct block *b, point_t *p);

#endif