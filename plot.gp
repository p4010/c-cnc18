# Comments in gnuplot scripts start with #

# Enable 
set grid

# Set lable for x axis
set xlab "Time (s)"
set ylab "Values (c.u.)"
set title "Axis commissioning charts"

# Plot data series, separating series with commas
# use backslash (\) for multiple lines
plot "tuning.txt" u 1:2 w l title "position",\
  "tuning.txt" u 1:8 w l title "saturation",\
  "tuning.txt" u 1:9 w l title "error"
