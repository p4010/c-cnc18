# Install prerequisites on Ubuntu 18.04 and 19.04

After a clean install, run the following for supporting development:

```bash
$ sudo apt update
$ sudo apt install build-essential \
$      git cmake clang libreadline8 libreadline-dev lldb
$ sudo update-alternatives --set cc /usr/bin/clang
$ sudo update-alternatives --set c++ /usr/bin/clang++
```
